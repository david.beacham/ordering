{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Ordering.Lattice
  (
  -- * Semilattices
  -- ** Join semilattices
    JoinSemiLattice(..)
  , join
  , joins1

  -- *** Generics
  , gjoin

  -- ** Bounded join semilattices
  , BoundedJoinSemiLattice
  , joins

  -- ** Newtype wrapper
  , Join(..)

  -- ** Meet semilattices
  , MeetSemiLattice(..)
  , meet
  , meets1

  -- *** Generics
  , gmeet

  -- ** Bounded meet semilattices
  , BoundedMeetSemiLattice
  , top
  , meets

  -- ** Newtype wrapper
  , Meet(..)

  -- * Lattices
  , Lattice

  -- * Bounded lattices
  , BoundedLattice
  ) where

import Control.Lens            (ala, iso, Wrapped(..), Rewrapped)
import Data.Coerce             (coerce)
import Data.Functor.Const      (Const(..))
import Data.Functor.Identity   (Identity(..))
import Data.Semigroup.Foldable (Foldable1(..))
import Data.Semigroup          (All(..), Arg(..), Any(..), Dual(..), First(..),
                                Last(..), Max(..), Min(..), Option(..),
                                Product(..), Sum(..)) 
import Data.Type.Equality      ((:~:))
import Data.Ord                (Down(..))
import Data.Void               (Void)
import GHC.Generics            (Generic(..), V1, U1(..), K1(..), M1(..),
                                (:+:)(..), (:*:)(..))
import Ordering.Extrema
import Ordering.PartialOrd

--------------------------------------------------------------------------------
-- * Semilattices
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- * Join semilattices
--------------------------------------------------------------------------------

-- | A @'JoinSemiLattice'@ is a @'PartialOrd'@ which has a unique least upper
-- bound, denoted @'join'@ or @'(\/)'@, for each pair of elements.
--
-- The operation is
--
-- * /Associative/
--
--   prop> (a \/ b) \/ c = a \/ (b \/ c)
--
-- * /Commutative/
--
--   prop> a \/ b = b \/ a
--
-- * /Idempotent/
--
--   prop> a \/ a = a
--
-- and so gives rise to a commutative semigroup which is provided by the
-- @'Join'@ wrapper.
class PartialOrd a => JoinSemiLattice a where
  (\/) :: a -> a -> a

-- | Synonym for @'(\/)'@.
join :: JoinSemiLattice a => a -> a -> a
join = (\/)

-- | Find the least upper bound of some non-empty set of values.
joins1 :: Foldable1 t => JoinSemiLattice a => t a -> a
joins1 = ala Join foldMap1

--------------------------------------------------------------------------------
-- *** Generics
--------------------------------------------------------------------------------

gjoin
  :: forall a
   . Generic a
  => JoinSemiLattice (Rep a ())
  => a -> a -> a
gjoin = \x y -> to (join (from' x) (from' y))
  where
  from' :: a -> Rep a ()
  from' = from

instance JoinSemiLattice (V1 a) where
  (\/) a _ = a

instance JoinSemiLattice (U1 a) where
  (\/) U1 U1 = U1

-- |
instance JoinSemiLattice c => JoinSemiLattice (K1 i c p) where
  (\/) = coerce ((\/) :: c -> c -> c)

-- |
instance JoinSemiLattice (f p) => JoinSemiLattice (M1 i c f p) where
  (\/) = coerce ((\/) :: f p -> f p -> f p)

-- | Ordinal sum ordering to match the @'Ord' ('(:+:)' f g p)@ instance.
instance (JoinSemiLattice (f p), JoinSemiLattice (g p))
  => JoinSemiLattice ((f :+: g) p) where

  (\/)   (L1 x)    (L1 y) = L1 (x \/ y)
  (\/)   (L1 _)  y@(R1 _) = y
  (\/) x@(R1 _)    (L1 _) = x
  (\/)   (R1 x)    (R1 y) = R1 (x \/ y)

-- | Lexicographic to match the @'Ord' ('(:*:)' f g p)@ instance.
instance (JoinSemiLattice (f p), BoundedJoinSemiLattice (g p))
  => JoinSemiLattice ((f :*: g) p) where

  (\/) x@(ax :*: bx) y@(ay :*: by) =
    case partialCompare ax ay of
      Just LT -> x
      Just EQ -> ax :*: (\/) bx by
      Just GT -> y
      Nothing -> (ax \/ ay) :*: bottom
  {-# inline (\/) #-}

--------------------------------------------------------------------------------
-- *** Instances
--------------------------------------------------------------------------------

-- | Get a @'JoinSemiLattice'@ instance for an @a@ with an @'Ord'@ instance.
instance Ord a => JoinSemiLattice (WrappedOrd a) where
  (\/) = max

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd (a :~: b) instance JoinSemiLattice (a :~: b)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd () instance JoinSemiLattice ()

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b)
  instance (JoinSemiLattice a,
            BoundedJoinSemiLattice b)
  => JoinSemiLattice (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (JoinSemiLattice a,
            BoundedJoinSemiLattice b, BoundedJoinSemiLattice c)
  => JoinSemiLattice (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (JoinSemiLattice a,
            BoundedJoinSemiLattice b, BoundedJoinSemiLattice c,
            BoundedJoinSemiLattice d)
  => JoinSemiLattice (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (JoinSemiLattice a,
            BoundedJoinSemiLattice b, BoundedJoinSemiLattice c,
            BoundedJoinSemiLattice d, BoundedJoinSemiLattice e)
  => JoinSemiLattice (a, b, c, d, e)

-- |
deriving newtype instance JoinSemiLattice All

-- |
deriving newtype instance JoinSemiLattice Any

-- | Derived via @'WrappedOrd'@.
instance (PartialOrd a, Ord a) => JoinSemiLattice (Arg a b) where
  (\/) = max

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Bool instance JoinSemiLattice Bool

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Char instance JoinSemiLattice Char

-- | Newtype derived.
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Const a b)

-- | Reversing the partial order in a @'MeetSemiLattice'@ gives rise to a
-- @'JoinSemiLattice'@.
instance MeetSemiLattice a => JoinSemiLattice (Down a) where
  (\/) = coerce ((/\) :: a -> a -> a)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Dual a)

-- | Generalises @'Ord' ('Either' a b)@.
instance (JoinSemiLattice a, JoinSemiLattice b) => JoinSemiLattice (Either a b) where
  (\/)   (Left x)    (Left y)  = Left (x \/ y)
  (\/)   (Left _)  y@(Right _) = y
  (\/) x@(Right _)   (Left _)  = x
  (\/)   (Right x)   (Right y) = Right (x \/ y)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (First a)

-- | Newtype derived.
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Identity a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Int instance JoinSemiLattice Int

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Integer instance JoinSemiLattice Integer

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Last a)

-- | Generalises @'Ord' ('Maybe' a)@.
instance JoinSemiLattice a => JoinSemiLattice (Maybe a) where
  (\/)    Nothing  x       = x
  (\/) x@(Just _) Nothing  = x
  (\/)   (Just x) (Just y) = Just (x \/ y)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Max a)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Min a)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Option a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Ordering instance JoinSemiLattice Ordering

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Product a)

-- |
deriving newtype instance JoinSemiLattice a => JoinSemiLattice (Sum a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Void instance JoinSemiLattice Void

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Word instance JoinSemiLattice Word

instance (JoinSemiLattice a,
          BoundedJoinSemiLattice b)
  => JoinSemiLattice (Lexicographic (a, b)) where

  (\/)
    x@(Lexicographic (a1, b1))
    y@(Lexicographic (a2, b2)) =
      case partialCompare a1 a2 of
        Just LT -> y
        Just EQ -> Lexicographic (a1, b1 \/ b2)
        Just GT -> x
        Nothing -> Lexicographic (a1 \/ a2, bottom)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a,
          BoundedJoinSemiLattice b, BoundedJoinSemiLattice c)
  => JoinSemiLattice (Lexicographic (a, b, c)) where

  (\/)
    x@(Lexicographic (a1, b1, c1))
    y@(Lexicographic (a2, b2, c2)) =
      case partialCompare a1 a2 of
        Just LT -> y
        Just EQ ->
          let Lexicographic (b', c') =
                (\/) (Lexicographic (b1, c1))
                     (Lexicographic (b2, c2))
           in Lexicographic (a1, b', c')
        Just GT -> x
        Nothing -> Lexicographic (a1 \/ a2, bottom, bottom)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a,
          BoundedJoinSemiLattice b, BoundedJoinSemiLattice c,
          BoundedJoinSemiLattice d)
  => JoinSemiLattice (Lexicographic (a, b, c, d)) where

  (\/)
    x@(Lexicographic (a1, b1, c1, d1))
    y@(Lexicographic (a2, b2, c2, d2)) =
      case partialCompare a1 a2 of
        Just LT -> y
        Just EQ ->
          let Lexicographic (b', c', d') =
                (\/) (Lexicographic (b1, c1, d1))
                     (Lexicographic (b2, c2, d2))
           in Lexicographic (a1, b', c', d')
        Just GT -> x
        Nothing -> Lexicographic (a1 \/ a2, bottom, bottom, bottom)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a,
          BoundedJoinSemiLattice b, BoundedJoinSemiLattice c,
          BoundedJoinSemiLattice d, BoundedJoinSemiLattice e)
  => JoinSemiLattice (Lexicographic (a, b, c, d, e)) where

  (\/)
    x@(Lexicographic (a1, b1, c1, d1, e1))
    y@(Lexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare a1 a2 of
        Just LT -> y
        Just EQ ->
          let Lexicographic (b', c', d', e') =
                (\/) (Lexicographic (b1, c1, d1, e1))
                     (Lexicographic (b2, c2, d2, e2))
           in Lexicographic (a1, b', c', d', e')
        Just GT -> x
        Nothing -> Lexicographic (a1 \/ a2, bottom, bottom, bottom, bottom)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice b,
          BoundedJoinSemiLattice a)
  => JoinSemiLattice (CoLexicographic (a, b)) where

  (\/)
    x@(CoLexicographic (a1, b1))
    y@(CoLexicographic (a2, b2)) =
      case partialCompare b1 b2 of
        Just LT -> y
        Just EQ -> CoLexicographic (a1 \/ a2, b1)
        Just GT -> x
        Nothing -> CoLexicographic (bottom, b1 \/ b2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice c,
          BoundedJoinSemiLattice b, BoundedJoinSemiLattice a)
  => JoinSemiLattice (CoLexicographic (a, b, c)) where

  (\/)
    x@(CoLexicographic (a1, b1, c1))
    y@(CoLexicographic (a2, b2, c2)) =
      case partialCompare c1 c2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b') =
                (\/) (CoLexicographic (a1, b1))
                     (CoLexicographic (a2, b2))
           in CoLexicographic (a', b', c1)
        Just GT -> x
        Nothing -> CoLexicographic (bottom, bottom, c1 \/ c2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice d,
          BoundedJoinSemiLattice c, BoundedJoinSemiLattice b,
          BoundedJoinSemiLattice a)
  => JoinSemiLattice (CoLexicographic (a, b, c, d)) where

  (\/)
    x@(CoLexicographic (a1, b1, c1, d1))
    y@(CoLexicographic (a2, b2, c2, d2)) =
      case partialCompare d1 d2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b', c') =
                (\/) (CoLexicographic (a1, b1, c1))
                     (CoLexicographic (a2, b2, c2))
           in CoLexicographic (a', b', c', d1)
        Just GT -> x
        Nothing -> CoLexicographic (bottom, bottom, bottom, d1 \/ d2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice e,
          BoundedJoinSemiLattice d, BoundedJoinSemiLattice c,
          BoundedJoinSemiLattice b, BoundedJoinSemiLattice a)
  => JoinSemiLattice (CoLexicographic (a, b, c, d, e)) where

  (\/)
    x@(CoLexicographic (a1, b1, c1, d1, e1))
    y@(CoLexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare e1 e2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b', c', d') =
                (\/) (CoLexicographic (a1, b1, c1, d1))
                     (CoLexicographic (a2, b2, c2, d2))
           in CoLexicographic (a', b', c', d', e1)
        Just GT -> x
        Nothing -> CoLexicographic (bottom, bottom, bottom, bottom, e1 \/ e2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a, JoinSemiLattice b)
  => JoinSemiLattice (ProductOrder (a, b)) where

  (\/)
    (ProductOrder (a1, b1))
    (ProductOrder (a2, b2)) =
      ProductOrder (a1 \/ a2, b1 \/ b2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a, JoinSemiLattice b, JoinSemiLattice c)
  => JoinSemiLattice (ProductOrder (a, b, c)) where

  (\/)
    (ProductOrder (a1, b1, c1))
    (ProductOrder (a2, b2, c2)) =
      ProductOrder (a1 \/ a2, b1 \/ b2, c1 \/ c2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a, JoinSemiLattice b, JoinSemiLattice c,
          JoinSemiLattice d)
  => JoinSemiLattice (ProductOrder (a, b, c, d)) where

  (\/)
    (ProductOrder (a1, b1, c1, d1))
    (ProductOrder (a2, b2, c2, d2)) =
      ProductOrder (a1 \/ a2, b1 \/ b2, c1 \/ c2, d1 \/ d2)
  {-# INLINE (\/) #-}

instance (JoinSemiLattice a, JoinSemiLattice b, JoinSemiLattice c,
          JoinSemiLattice d, JoinSemiLattice e)
  => JoinSemiLattice (ProductOrder (a, b, c, d, e)) where

  (\/)
    (ProductOrder (a1, b1, c1, d1, e1))
    (ProductOrder (a2, b2, c2, d2, e2)) =
      ProductOrder (a1 \/ a2, b1 \/ b2, c1 \/ c2, d1 \/ d2, e1 \/ e2)
  {-# INLINE (\/) #-}

-- | Newtype derived.
deriving newtype instance JoinSemiLattice a
  => JoinSemiLattice (WrappedBounded a)

--------------------------------------------------------------------------------
-- ** Bounded join semilattices
--------------------------------------------------------------------------------

-- | A @'BoundedJoinSemiLattice'@ is a @'JoinSemiLattice'@ with a least
-- element, 'bottom'.
type BoundedJoinSemiLattice a = (LeastElement a, JoinSemiLattice a)

-- | The least upper bound of some set of values. If the set is empty then
-- 'bottom' is returned.
joins :: forall t a. Foldable t => BoundedJoinSemiLattice a => t a -> a
joins = ala Join foldMap

--------------------------------------------------------------------------------
-- ** Newtype wrapper
--------------------------------------------------------------------------------

-- | Wrapper providing @'Semigroup'@ and @'Monoid'@ instances formed by
-- @'join'@ and @'bottom'@ from @'JoinSemiLattice'@ and
-- @'BoundedJoinSemiLattice'@, respectively.
newtype Join a = Join a
  deriving (Show, Read, Eq, Ord)

instance (t ~ Join a) => Rewrapped (Join a) t
instance Wrapped (Join a) where
  type Unwrapped (Join a) = a
  _Wrapped' = iso coerce coerce
  {-# INLINE _Wrapped' #-}

instance JoinSemiLattice a => Semigroup (Join a) where
  (<>) = coerce ((\/) :: a -> a -> a)

instance BoundedJoinSemiLattice a => Monoid (Join a) where
  mempty = Join bottom

--------------------------------------------------------------------------------
-- ** Meet semilattices
--------------------------------------------------------------------------------


-- | A @'MeetSemiLattice'@ is a @'PartialOrd'@ which has a unique greatest
-- lower bound, denoted @'meet'@ or @'(/\)'@, for each pair of elements.
--
-- The operation is
--
-- * /Associative/
--
--   prop> (a /\ b) /\ c = a /\ (b /\ c)
--
-- * /Commutative/
--
--   prop> a /\ b = b /\ a
--
-- * /Idempotent/
--
--   prop> a /\ a = a
--
-- and so gives rise to a commutative semigroup which is provided by the
-- @'Meet'@ wrapper.
class PartialOrd a => MeetSemiLattice a where
  (/\) :: a -> a -> a

-- | Synonym for @'(/\)'@.
meet :: MeetSemiLattice a => a -> a -> a
meet = (/\)

-- | Find the greatest lower bound of some non-empty set of values.
meets1 :: Foldable1 t => MeetSemiLattice a => t a -> a
meets1 = ala Meet foldMap1

--------------------------------------------------------------------------------
-- *** Generics
--------------------------------------------------------------------------------

gmeet
  :: forall a
   . Generic a
  => MeetSemiLattice (Rep a ())
  => a -> a -> a
gmeet = \x y -> to (meet (from' x) (from' y))
  where
  from' :: a -> Rep a ()
  from' = from

instance MeetSemiLattice (V1 a) where
  (/\) _ _ = undefined

instance MeetSemiLattice (U1 a) where
  (/\) U1 U1 = U1

-- |
instance MeetSemiLattice c => MeetSemiLattice (K1 i c p) where
  (/\) = coerce ((/\) :: c -> c -> c)

-- |
instance MeetSemiLattice (f p) => MeetSemiLattice (M1 i c f p) where
  (/\) = coerce ((/\) :: f p -> f p -> f p)

-- | Ordinal sum ordering to match the @'Ord' ('(:+:)' f g p)@ instance.
instance (MeetSemiLattice (f p), MeetSemiLattice (g p))
  => MeetSemiLattice ((f :+: g) p) where

  (/\)   (L1 x)    (L1 y) = L1 (x /\ y)
  (/\)   (L1 _)  y@(R1 _) = y
  (/\) x@(R1 _)    (L1 _) = x
  (/\)   (R1 x)    (R1 y) = R1 (x /\ y)

-- | Lexicographic to match the @'Ord' ('(:*:)' f g p)@ instance.
instance (MeetSemiLattice (f p), BoundedMeetSemiLattice (g p))
  => MeetSemiLattice ((f :*: g) p) where

  (/\) x@(ax :*: bx) y@(ay :*: by) =
    case partialCompare ax ay of
      Just LT -> y
      Just EQ -> ax :*: (/\) bx by
      Just GT -> x
      Nothing -> (ax /\ ay) :*: top
  {-# inline (/\) #-}

--------------------------------------------------------------------------------
-- *** Instances
--------------------------------------------------------------------------------

-- | Get a @'MeetSemiLattice'@ instance for an @a@ with an @'Ord'@ instance.
instance Ord a => MeetSemiLattice (WrappedOrd a) where
  (/\) = min

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd (a :~: b) instance MeetSemiLattice (a :~: b)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd () instance MeetSemiLattice ()

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b)
  instance (MeetSemiLattice a,
            BoundedMeetSemiLattice b)
  => MeetSemiLattice (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (MeetSemiLattice a,
            BoundedMeetSemiLattice b, BoundedMeetSemiLattice c)
  => MeetSemiLattice (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (MeetSemiLattice a,
            BoundedMeetSemiLattice b, BoundedMeetSemiLattice c,
            BoundedMeetSemiLattice d)
  => MeetSemiLattice (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (MeetSemiLattice a,
            BoundedMeetSemiLattice b, BoundedMeetSemiLattice c,
            BoundedMeetSemiLattice d, BoundedMeetSemiLattice e)
  => MeetSemiLattice (a, b, c, d, e)

-- |
deriving newtype instance MeetSemiLattice All

-- |
deriving newtype instance MeetSemiLattice Any

-- | Derived via @'WrappedOrd'@.
instance (PartialOrd a, Ord a) => MeetSemiLattice (Arg a b) where
  (/\) = min

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Bool instance MeetSemiLattice Bool

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Char instance MeetSemiLattice Char

-- | Newtype derived.
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Const a b)

-- | Reversing the partial order in a @'JoinSemiLattice'@ gives rise to a
-- @'MeetSemiLattice'@.
instance JoinSemiLattice a => MeetSemiLattice (Down a) where
  (/\) = coerce ((\/) :: a -> a -> a)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Dual a)

-- | Generalises @'Ord' ('Either' a b)@.
instance (MeetSemiLattice a, MeetSemiLattice b)
  => MeetSemiLattice (Either a b) where

  (/\)   (Left x)    (Left y)  = Left (x /\ y)
  (/\)   (Left _)  y@(Right _) = y
  (/\) x@(Right _)   (Left _)  = x
  (/\)   (Right x)   (Right y) = Right (x /\ y)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (First a)

-- | Newtype derived.
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Identity a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Int instance MeetSemiLattice Int

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Integer instance MeetSemiLattice Integer

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Last a)

-- | Generalises @'Ord' ('Maybe' a)@.
instance MeetSemiLattice a => MeetSemiLattice (Maybe a) where
  (/\)    Nothing  x       = x
  (/\) x@(Just _) Nothing  = x
  (/\)   (Just x) (Just y) = Just (x /\ y)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Max a)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Min a)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Option a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Ordering instance MeetSemiLattice Ordering

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Product a)

-- |
deriving newtype instance MeetSemiLattice a => MeetSemiLattice (Sum a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Void instance MeetSemiLattice Void

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Word instance MeetSemiLattice Word

instance (MeetSemiLattice a,
          BoundedMeetSemiLattice b)
  => MeetSemiLattice (Lexicographic (a, b)) where

  (/\)
    x@(Lexicographic (a1, b1))
    y@(Lexicographic (a2, b2)) =
      case partialCompare a1 a2 of
        Just LT -> x
        Just EQ -> Lexicographic (a1, b1 /\ b2)
        Just GT -> y
        Nothing -> Lexicographic (a1 /\ a2, top)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a,
          BoundedMeetSemiLattice b, BoundedMeetSemiLattice c)
  => MeetSemiLattice (Lexicographic (a, b, c)) where

  (/\)
    x@(Lexicographic (a1, b1, c1))
    y@(Lexicographic (a2, b2, c2)) =
      case partialCompare a1 a2 of
        Just LT -> x
        Just EQ ->
          let Lexicographic (b', c') =
                (/\) (Lexicographic (b1, c1))
                     (Lexicographic (b2, c2))
           in Lexicographic (a1, b', c')

        Just GT -> y
        Nothing -> Lexicographic (a1 /\ a2, top, top)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a,
          BoundedMeetSemiLattice b, BoundedMeetSemiLattice c, BoundedMeetSemiLattice d)
  => MeetSemiLattice (Lexicographic (a, b, c, d)) where

  (/\)
    x@(Lexicographic (a1, b1, c1, d1))
    y@(Lexicographic (a2, b2, c2, d2)) =
      case partialCompare a1 a2 of
        Just LT -> x
        Just EQ ->
          let Lexicographic (b', c', d') =
                (/\) (Lexicographic (b1, c1, d1))
                     (Lexicographic (b2, c2, d2))
           in Lexicographic (a1, b', c', d')

        Just GT -> y
        Nothing -> Lexicographic (a1 /\ a2, top, top, top)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a,
          BoundedMeetSemiLattice b, BoundedMeetSemiLattice c,
          BoundedMeetSemiLattice d, BoundedMeetSemiLattice e)
  => MeetSemiLattice (Lexicographic (a, b, c, d, e)) where

  (/\)
    x@(Lexicographic (a1, b1, c1, d1, e1))
    y@(Lexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare a1 a2 of
        Just LT -> x
        Just EQ ->
          let Lexicographic (b', c', d', e') =
                (/\) (Lexicographic (b1, c1, d1, e1))
                     (Lexicographic (b2, c2, d2, e2))
           in Lexicographic (a1, b', c', d', e')

        Just GT -> y
        Nothing -> Lexicographic (a1 /\ a2, top, top, top, top)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice b,
          BoundedMeetSemiLattice a)
  => MeetSemiLattice (CoLexicographic (a, b)) where

  (/\)
    x@(CoLexicographic (a1, b1))
    y@(CoLexicographic (a2, b2)) =
      case partialCompare b1 b2 of
        Just LT -> y
        Just EQ -> CoLexicographic (a1 /\ a2, b1)
        Just GT -> x
        Nothing -> CoLexicographic (top, b1 /\ b2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice c,
          BoundedMeetSemiLattice b, BoundedMeetSemiLattice a)
  => MeetSemiLattice (CoLexicographic (a, b, c)) where

  (/\)
    x@(CoLexicographic (a1, b1, c1))
    y@(CoLexicographic (a2, b2, c2)) =
      case partialCompare c1 c2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b') =
                (/\) (CoLexicographic (a1, b1))
                     (CoLexicographic (a2, b2))
           in CoLexicographic (a', b', c1)
        Just GT -> x
        Nothing -> CoLexicographic (top, top, c1 /\ c2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice d,
          BoundedMeetSemiLattice c, BoundedMeetSemiLattice b,
          BoundedMeetSemiLattice a)
  => MeetSemiLattice (CoLexicographic (a, b, c, d)) where

  (/\)
    x@(CoLexicographic (a1, b1, c1, d1))
    y@(CoLexicographic (a2, b2, c2, d2)) =
      case partialCompare d1 d2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b', c') =
                (/\) (CoLexicographic (a1, b1, c1))
                     (CoLexicographic (a2, b2, c2))
           in CoLexicographic (a', b', c', d1)
        Just GT -> x
        Nothing -> CoLexicographic (top, top, top, d1 /\ d2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice e,
          BoundedMeetSemiLattice d, BoundedMeetSemiLattice c,
          BoundedMeetSemiLattice b, BoundedMeetSemiLattice a)
  => MeetSemiLattice (CoLexicographic (a, b, c, d, e)) where

  (/\)
    x@(CoLexicographic (a1, b1, c1, d1, e1))
    y@(CoLexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare e1 e2 of
        Just LT -> y
        Just EQ ->
          let CoLexicographic (a', b', c', d') =
                (/\) (CoLexicographic (a1, b1, c1, d1))
                     (CoLexicographic (a2, b2, c2, d2))
           in CoLexicographic (a', b', c', d', e1)
        Just GT -> x
        Nothing -> CoLexicographic (top, top, top, top, e1 /\ e2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a, MeetSemiLattice b)
  => MeetSemiLattice (ProductOrder (a, b)) where

  (/\)
    (ProductOrder (a1, b1))
    (ProductOrder (a2, b2)) =
      ProductOrder (a1 /\ a2, b1 /\ b2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a, MeetSemiLattice b, MeetSemiLattice c)
  => MeetSemiLattice (ProductOrder (a, b, c)) where

  (/\)
    (ProductOrder (a1, b1, c1))
    (ProductOrder (a2, b2, c2)) =
      ProductOrder (a1 /\ a2, b1 /\ b2, c1 /\ c2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a, MeetSemiLattice b, MeetSemiLattice c,
          MeetSemiLattice d)
  => MeetSemiLattice (ProductOrder (a, b, c, d)) where

  (/\)
    (ProductOrder (a1, b1, c1, d1))
    (ProductOrder (a2, b2, c2, d2)) =
      ProductOrder (a1 /\ a2, b1 /\ b2, c1 /\ c2, d1 /\ d2)
  {-# INLINE (/\) #-}

instance (MeetSemiLattice a, MeetSemiLattice b, MeetSemiLattice c,
          MeetSemiLattice d, MeetSemiLattice e)
  => MeetSemiLattice (ProductOrder (a, b, c, d, e)) where

  (/\)
    (ProductOrder (a1, b1, c1, d1, e1))
    (ProductOrder (a2, b2, c2, d2, e2)) =
      ProductOrder (a1 /\ a2, b1 /\ b2, c1 /\ c2, d1 /\ d2, e1 /\ e2)
  {-# INLINE (/\) #-}

-- | Newtype derived.
deriving newtype instance MeetSemiLattice a
  => MeetSemiLattice (WrappedBounded a)

--------------------------------------------------------------------------------
-- ** Bounded meet semilattices
--------------------------------------------------------------------------------

-- | A @'BoundedMeetSemiLattice'@ is a @'MeetSemiLattice'@ with a greatest
-- element, 'top'.
type BoundedMeetSemiLattice a = (GreatestElement a, MeetSemiLattice a)

-- | The greatest lower bound of some set of values. If the set is empty then
-- 'top' is returned.
meets :: forall t a. Foldable t => BoundedMeetSemiLattice a => t a -> a
meets = ala Meet foldMap

--------------------------------------------------------------------------------
-- ** Newtype wrapper
--------------------------------------------------------------------------------

-- | Wrapper providing @'Semigroup'@ and @'Monoid'@ instances formed by
-- @'meet'@ and @'top'@ from @'MeetSemiLattice'@ and
-- @'BoundedMeetSemiLattice'@, respectively.
newtype Meet a = Meet a
  deriving (Show, Read, Eq, Ord)

instance (t ~ Meet a) => Rewrapped (Meet a) t
instance Wrapped (Meet a) where
  type Unwrapped (Meet a) = a
  _Wrapped' = iso coerce coerce
  {-# INLINE _Wrapped' #-}

instance MeetSemiLattice a => Semigroup (Meet a) where
  (<>) = coerce ((/\) :: a -> a -> a)

instance BoundedMeetSemiLattice a => Monoid (Meet a) where
  mempty = Meet top

--------------------------------------------------------------------------------
-- * Lattices
--------------------------------------------------------------------------------

-- | A @'PartialOrd'@ which is both a @'JoinSemiLattice'@ and a
-- @'MeetSemiLattice'@ is a @'Lattice'@.
--
-- The absorption laws
-- 
-- prop> a /\ (a \/ b) = a
-- prop> a \/ (a /\ b) = a
--
-- are automatically satisfied.
type Lattice a = (JoinSemiLattice a, MeetSemiLattice a)

--------------------------------------------------------------------------------
-- * Bounded lattices
--------------------------------------------------------------------------------

-- | A bounded lattice is both a @'BoundedJoinSemiLattice'@ and a
-- @'BoundedMeetSemiLattice'@.  
type BoundedLattice a = (BoundedJoinSemiLattice a, BoundedMeetSemiLattice a)
