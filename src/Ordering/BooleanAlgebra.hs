{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Ordering.BooleanAlgebra
  (
  -- * Boolean algebras
    BooleanAlgebra(..)
  ) where

import Data.Type.Equality ((:~:))
import Ordering.HeytingAlgebra
import Ordering.PartialOrd

--------------------------------------------------------------------------------
-- * Boolean algebras
--------------------------------------------------------------------------------

class BiHeytingAlgebra a => BooleanAlgebra a where
  complement :: a -> a
  complement = pseudoComplement

  {-# MINIMAL #-}

--------------------------------------------------------------------------------
-- ** Instances
--------------------------------------------------------------------------------

instance (a ~ b) => BooleanAlgebra (a :~: b)
instance BooleanAlgebra ()
instance BooleanAlgebra Bool

instance (BooleanAlgebra a, BooleanAlgebra b)
  => (BooleanAlgebra (ProductOrder (a, b))) where

instance (BooleanAlgebra a, BooleanAlgebra b, BooleanAlgebra c)
  => (BooleanAlgebra (ProductOrder (a, b, c))) where

instance (BooleanAlgebra a, BooleanAlgebra b, BooleanAlgebra c,
          BooleanAlgebra d)
  => (BooleanAlgebra (ProductOrder (a, b, c, d))) where

instance (BooleanAlgebra a, BooleanAlgebra b, BooleanAlgebra c,
          BooleanAlgebra d, BooleanAlgebra e)
  => (BooleanAlgebra (ProductOrder (a, b, c, d, e))) where
