{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Ordering.PartialOrd
  (
  -- * Partial orders
    PartialOrd(..)

  -- ** Generics
  , gpartialCompare

  -- * Functions
  , partialComparing

  -- * `Ord` wrapper
  , WrappedOrd(..)

  -- * Product orderings
  -- ** Lexicographic
  , Lexicographic(..)

  -- ** Co-lexicographic
  , CoLexicographic(..)

  -- ** Product order
  , ProductOrder(..)
  ) where

import Control.Lens          (iso, Wrapped(..), Rewrapped)
import Data.Coerce           (coerce)
import Data.Functor.Const    (Const(..))
import Data.Functor.Identity (Identity(..))
import Data.Semigroup        (All(..), Arg(..), Any(..), Dual(..), First(..),
                              Last(..), Max(..), Min(..), Option(..),
                              Product(..), Sum(..)) 
import Data.Type.Equality    ((:~:))
import Data.Ord              (Down(..))
import Data.Void             (Void)
import GHC.Generics          (Generic(..), V1, U1(..), K1(..), M1(..),
                              (:+:)(..), (:*:)(..))

--------------------------------------------------------------------------------
-- * Partial orders
--------------------------------------------------------------------------------

-- |
class Eq a => PartialOrd a where
  partialCompare :: a -> a -> Maybe Ordering
  partialCompare x y
    | x ==  y   = Just EQ
    | x <=? y   = Just LT
    | y <=? x   = Just GT
    | otherwise = Nothing

  comparable :: a -> a -> Bool
  comparable x y = x <=? y || y <=? x

  (<?) :: a -> a -> Bool
  (<?) x y = case partialCompare x y of
               Just LT -> True
               _       -> False

  (<=?) :: a -> a -> Bool
  (<=?) x y = case partialCompare x y of
                Just LT -> True
                Just EQ -> True
                _       -> False

  (>?) :: a -> a -> Bool
  (>?) x y = case partialCompare x y of
                Just GT -> True
                _       -> False

  (>=?) :: a -> a -> Bool
  (>=?) x y = case partialCompare x y of
                Just EQ -> True
                Just GT -> True
                _       -> False

  {-# MINIMAL partialCompare | (<=?) #-}

--------------------------------------------------------------------------------
-- ** Generics
--------------------------------------------------------------------------------

-- |
gpartialCompare
  :: forall a
   . Generic a
  => PartialOrd (Rep a ())
  => a
  -> a
  -> Maybe Ordering
gpartialCompare = partialComparing (from :: a -> Rep a ())

-- | 
instance PartialOrd (V1 a) where
  partialCompare _ _ = Just EQ

-- | 
instance PartialOrd (U1 a) where
  partialCompare _ _ = Just EQ

-- |
deriving newtype instance PartialOrd c => PartialOrd (K1 i c p)

-- |
deriving newtype instance PartialOrd (f p) => PartialOrd (M1 i c f p)

-- | Ordinal sum ordering to match the @'Ord' ('(:+:)' f g p)@ instance.
instance (PartialOrd (f p), PartialOrd (g p)) => PartialOrd ((f :+: g) p) where
  partialCompare (L1 x) (L1 y) = partialCompare x y
  partialCompare (L1 _) (R1 _) = Just LT
  partialCompare (R1 _) (L1 _) = Just GT
  partialCompare (R1 x) (R1 y) = partialCompare x y

-- | Lexicographic to match the @'Ord' ('(:*:)' f g p)@ instance.
instance (PartialOrd (f p), PartialOrd (g p)) => PartialOrd ((f :*: g) p) where
  partialCompare (x1 :*: y1) (x2 :*: y2) =
    case partialCompare x1 x2 of
      Just EQ -> partialCompare y1 y2
      x       -> x

--------------------------------------------------------------------------------
-- ** Other instances
--------------------------------------------------------------------------------

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd (a :~: b) instance PartialOrd (a :~: b)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd () instance PartialOrd ()

-- | Derived via @'WrappedOrd'@.
deriving via Lexicographic (a, b)
  instance (PartialOrd a, PartialOrd b)
  => PartialOrd (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (PartialOrd a, PartialOrd b, PartialOrd c)
  => PartialOrd (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d)
  => PartialOrd (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d, PartialOrd e)
  => PartialOrd (a, b, c, d, e)

-- |
deriving newtype instance PartialOrd All

-- |
deriving newtype instance PartialOrd Any

-- | Derived via @'WrappedOrd'@.
instance PartialOrd a => PartialOrd (Arg a b) where
  partialCompare (Arg x _) (Arg y _) = partialCompare x y

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Bool instance PartialOrd Bool

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Char instance PartialOrd Char

-- | Newtype derived.
deriving newtype instance PartialOrd a => PartialOrd (Const a b)

-- | Generalises @'Ord' ('Down' a)@.
instance PartialOrd a => PartialOrd (Down a) where
  partialCompare = coerce ((flip partialCompare) :: a -> a -> Maybe Ordering)

  comparable = coerce (comparable :: a -> a -> Bool) 

  (<?)  = coerce (flip (<?)  :: a -> a -> Bool)
  (<=?) = coerce (flip (<=?) :: a -> a -> Bool)
  (>?)  = coerce (flip (>?)  :: a -> a -> Bool)
  (>=?) = coerce (flip (>=?) :: a -> a -> Bool)

-- |
deriving newtype instance PartialOrd a => PartialOrd (Dual a)

-- | Generalises @'Ord' ('Either' a b)@.
instance (PartialOrd a, PartialOrd b) => PartialOrd (Either a b) where
  partialCompare (Left x)  (Left y)  = partialCompare x y
  partialCompare (Left _)  (Right _) = Just LT
  partialCompare (Right _) (Left _)  = Just GT
  partialCompare (Right x) (Right y) = partialCompare x y

-- |
deriving newtype instance PartialOrd a => PartialOrd (First a)

-- | Newtype derived.
deriving newtype instance PartialOrd a => PartialOrd (Identity a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Int instance PartialOrd Int

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Integer instance PartialOrd Integer

-- |
deriving newtype instance PartialOrd a => PartialOrd (Last a)

-- | Generalises @'Ord' ('Maybe' a)@.
instance PartialOrd a => PartialOrd (Maybe a) where
  partialCompare Nothing  Nothing  = Just EQ
  partialCompare Nothing  (Just _) = Just LT
  partialCompare (Just _) Nothing  = Just GT
  partialCompare (Just x) (Just y) = partialCompare x y

-- |
deriving newtype instance PartialOrd a => PartialOrd (Max a)

-- |
deriving newtype instance PartialOrd a => PartialOrd (Min a)

-- |
deriving newtype instance PartialOrd a => PartialOrd (Option a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Ordering instance PartialOrd Ordering

-- |
deriving newtype instance PartialOrd a => PartialOrd (Product a)

-- |
deriving newtype instance PartialOrd a => PartialOrd (Sum a)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Void instance PartialOrd Void

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Word instance PartialOrd Word

--------------------------------------------------------------------------------
-- * Functions
--------------------------------------------------------------------------------

-- | Compare two values, @x, y :: b@, by first projecting them into the partial
-- order defined on @a@.
--
-- > comparing f x y = compare (f x) (f y)
partialComparing :: PartialOrd a => (b -> a) -> b -> b -> Maybe Ordering
partialComparing f x y = partialCompare (f x) (f y)

--------------------------------------------------------------------------------
-- * `Ord` wrapper
--------------------------------------------------------------------------------

-- | Get a @'PartialOrd'@ instance for any @a@ with an @'Ord'@ instance.
newtype WrappedOrd a = WrappedOrd a
  deriving (Show, Read, Eq, Ord, Bounded)

instance (t ~ WrappedOrd a) => Rewrapped (WrappedOrd a) t
instance Wrapped (WrappedOrd a) where
  type Unwrapped (WrappedOrd a) = a
  _Wrapped' = iso coerce coerce
  {-# INLINE _Wrapped' #-}

-- | Get a @'PartialOrd'@ instance for any wrapped @a@ with an @'Ord'@
-- instance.
instance Ord a => PartialOrd (WrappedOrd a) where
  partialCompare x y = Just (compare x y)

  comparable _ _ = True

  (<?)  = (<)
  (<=?) = (<=)
  (>?)  = (>)
  (>=?) = (>=)

--------------------------------------------------------------------------------
-- * Product orderings
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- ** Lexicographic
--------------------------------------------------------------------------------

-- | Enable partial orderings on tuples with correct constratints.
--
-- prop> (a1,b1) <= (a2,b2) <==> a1 < a2 \/ (a1 == a2 /\ b1 <= b2)
newtype Lexicographic a = Lexicographic a
  deriving (Show, Read, Eq)

--------------------------------------------------------------------------------
-- *** Instances
--------------------------------------------------------------------------------

instance (PartialOrd a, PartialOrd b)
  => PartialOrd (Lexicographic (a, b)) where

  partialCompare
    (Lexicographic (a1, b1))
    (Lexicographic (a2, b2)) =
      case partialCompare a1 a2 of
        Just EQ -> partialCompare b1 b2
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c)
  => PartialOrd (Lexicographic (a, b, c)) where

  partialCompare
    (Lexicographic (a1, b1, c1))
    (Lexicographic (a2, b2, c2)) =
      case partialCompare a1 a2 of
        Just EQ -> partialCompare (Lexicographic (b1, c1))
                                  (Lexicographic (b2, c2))
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d)
  => PartialOrd (Lexicographic (a, b, c, d)) where

  partialCompare
    (Lexicographic (a1, b1, c1, d1))
    (Lexicographic (a2, b2, c2, d2)) =
      case partialCompare a1 a2 of
        Just EQ -> partialCompare (Lexicographic (b1, c1, d1))
                                  (Lexicographic (b2, c2, d2))
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d, PartialOrd e)
  => PartialOrd (Lexicographic (a, b, c, d, e)) where

  partialCompare
    (Lexicographic (a1, b1, c1, d1, e1))
    (Lexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare a1 a2 of
        Just EQ -> partialCompare (Lexicographic (b1, c1, d1, e1))
                                  (Lexicographic (b2, c2, d2, e2))
        x       -> x
  {-# INLINE partialCompare #-}

--------------------------------------------------------------------------------
-- ** Co-Lexicographic
--------------------------------------------------------------------------------

-- | Enable co-lexicographic partial orderings on tuples with correct
-- constraints. The co-lexicographical order is obtained by reading elements
-- from right-to-left rather than left-to-right as in the 'Lexicographic'
-- order.
--
-- prop> (a1,b1) <= (a2,b2) <==> b1 < b2 \/ (b1 == b2 /\ a1 <= a2)
newtype CoLexicographic a = CoLexicographic a
  deriving (Show, Read, Eq)

--------------------------------------------------------------------------------
-- *** Instances
--------------------------------------------------------------------------------

instance (PartialOrd a, PartialOrd b)
  => PartialOrd (CoLexicographic (a, b)) where

  partialCompare
    (CoLexicographic (a1, b1))
    (CoLexicographic (a2, b2)) =
      case partialCompare b1 b2 of
        Just EQ -> partialCompare a1 a2
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c)
  => PartialOrd (CoLexicographic (a, b, c)) where

  partialCompare
    (CoLexicographic (a1, b1, c1))
    (CoLexicographic (a2, b2, c2)) =
      case partialCompare c1 c2 of
        Just EQ -> partialCompare (CoLexicographic (a1, b1))
                                  (CoLexicographic (a2, b2))
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d)
  => PartialOrd (CoLexicographic (a, b, c, d)) where

  partialCompare
    (CoLexicographic (a1, b1, c1, d1))
    (CoLexicographic (a2, b2, c2, d2)) =
      case partialCompare d1 d2 of
        Just EQ -> partialCompare (CoLexicographic (a1, b1, c1))
                                  (CoLexicographic (a2, b2, c2))
        x       -> x
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d, PartialOrd e)
  => PartialOrd (CoLexicographic (a, b, c, d, e)) where

  partialCompare
    (CoLexicographic (a1, b1, c1, d1, e1))
    (CoLexicographic (a2, b2, c2, d2, e2)) =
      case partialCompare e1 e2 of
        Just EQ -> partialCompare (CoLexicographic (a1, b1, c1, d1))
                                  (CoLexicographic (a2, b2, c2, d2))
        x       -> x
  {-# INLINE partialCompare #-}

--------------------------------------------------------------------------------
-- ** Product
--------------------------------------------------------------------------------

-- | 
newtype ProductOrder a = ProductOrder a
  deriving (Show, Read, Eq)

--------------------------------------------------------------------------------
-- *** Instances
--------------------------------------------------------------------------------

instance (PartialOrd a, PartialOrd b)
  => PartialOrd (ProductOrder (a, b)) where

  partialCompare
    (ProductOrder (a1, b1))
    (ProductOrder (a2, b2)) =
      case partialCompare a1 a2 of
        Just EQ             -> partialCompare b1 b2
        Just LT | b1 <=? b2 -> Just LT
        Just GT | b1 >=? b2 -> Just GT
        _                   -> Nothing
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c)
  => PartialOrd (ProductOrder (a, b, c)) where

  partialCompare
    (ProductOrder (a1, b1, c1))
    (ProductOrder (a2, b2, c2)) =
      let p1 = ProductOrder (b1, c1)
          p2 = ProductOrder (b2, c2)
       in case partialCompare a1 a2 of
            Just EQ             -> partialCompare p1 p2
            Just LT | p1 <=? p2 -> Just LT
            Just GT | p1 >=? p2 -> Just GT
            _                   -> Nothing
  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d)
  => PartialOrd (ProductOrder (a, b, c, d)) where

  partialCompare
    (ProductOrder (a1, b1, c1, d1))
    (ProductOrder (a2, b2, c2, d2)) =
      let p1 = ProductOrder (b1, c1, d1)
          p2 = ProductOrder (b2, c2, d2)
       in case partialCompare a1 a2 of
            Just EQ             -> partialCompare p1 p2
            Just LT | p1 <=? p2 -> Just LT
            Just GT | p1 >=? p2 -> Just GT
            _                   -> Nothing

  {-# INLINE partialCompare #-}

instance (PartialOrd a, PartialOrd b, PartialOrd c, PartialOrd d, PartialOrd e)
  => PartialOrd (ProductOrder (a, b, c, d, e)) where

  partialCompare
    (ProductOrder (a1, b1, c1, d1, e1))
    (ProductOrder (a2, b2, c2, d2, e2)) =
      let p1 = ProductOrder (b1, c1, d1, e1)
          p2 = ProductOrder (b2, c2, d2, e2)
       in case partialCompare a1 a2 of
            Just EQ             -> partialCompare p1 p2
            Just LT | p1 <=? p2 -> Just LT
            Just GT | p1 >=? p2 -> Just GT
            _                   -> Nothing

  {-# INLINE partialCompare #-}
