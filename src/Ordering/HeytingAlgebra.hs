{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Ordering.HeytingAlgebra
  (
  -- * Heyting algebras
    HeytingAlgebra(..)
  , implies

  -- ** Generics
  , gimplies

  -- * Co-Heyting algebras
  , CoHeytingAlgebra(..)
  , subtracts

  -- ** Generics
  , gsubtracts

  -- * Bi-Heyting algebras
  , BiHeytingAlgebra
  ) where

import Data.Coerce             (coerce)
import Data.Functor.Const      (Const(..))
import Data.Functor.Identity   (Identity(..))
import Data.Semigroup          (All(..), Any(..))
import Data.Type.Equality      ((:~:))
import Data.Ord                (Down(..))
import GHC.Generics            (Generic(..), U1(..), K1(..), M1(..), (:+:)(..),
                                (:*:)(..))
import Ordering.Extrema
import Ordering.Lattice
import Ordering.PartialOrd

--------------------------------------------------------------------------------
-- * Heyting algebras
--------------------------------------------------------------------------------

-- | 
class BoundedLattice a => HeytingAlgebra a where
  -- | prop> a /\ x <=? b iff x <=? a ==> b
  (==>) :: a -> a -> a

  -- | prop> a /\ x <=? b iff x <=? b <== a
  (<==) :: a -> a -> a
  (<==) = flip (==>)

  -- | Greatest @x@ such that @a /\ x == bottom@.
  pseudoComplement :: a -> a
  pseudoComplement x = x ==> bottom
  {-# MINIMAL (==>) #-}

-- | Synonym for @'(==>)'@.
implies :: HeytingAlgebra a => a -> a -> a
implies = (==>)

--------------------------------------------------------------------------------
-- ** Generics
--------------------------------------------------------------------------------

gimplies
  :: forall a
   . Generic a
  => HeytingAlgebra (Rep a ())
  => a -> a -> a
gimplies = \x y -> to (implies (from' x) (from' y))
  where
  from' :: a -> Rep a ()
  from' = from

instance HeytingAlgebra (U1 a) where
  (==>) U1 U1 = U1

-- |
deriving newtype instance HeytingAlgebra c => HeytingAlgebra (K1 i c p)

-- |
deriving newtype instance HeytingAlgebra (f p) => HeytingAlgebra (M1 i c f p)

-- | Ordinal sum ordering to match the @'Ord' ('(:+:)' f g p)@ instance.
instance (HeytingAlgebra (f p), HeytingAlgebra (g p))
  => HeytingAlgebra ((f :+: g) p) where

  (==>) (L1 a) (L1 b) = L1 (a ==> b)
  (==>) (R1 a) (R1 b) = R1 (a ==> b)
  (==>) _      _      = top

-- | Lexicographic to match the @'Ord' ('(:*:)' f g p)@ instance.
instance (HeytingAlgebra (f p), HeytingAlgebra (g p))
  => HeytingAlgebra ((f :*: g) p) where

  (==>)
    x@(a1 :*: b1)
    y@(a2 :*: b2)
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              b' | a' == a2  = b1 ==> b2
                 | otherwise = top
           in a' :*: b'
  {-# INLINE (==>) #-}

--------------------------------------------------------------------------------
-- ** Instances
--------------------------------------------------------------------------------

-- | Get a @'HeytingAlgebra'@ instance for @'Bounded'@, @'Ord'@ @a@.
instance (Ord a, Bounded a)
  => HeytingAlgebra (WrappedBounded (WrappedOrd a)) where

  (==>) x y
    | x <= y    = top
    | otherwise = y

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via WrappedBounded (WrappedOrd (a :~: b))
  instance (a ~ b) => HeytingAlgebra (a :~: b)

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via WrappedBounded (WrappedOrd ())
  instance HeytingAlgebra ()

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b)
  instance (HeytingAlgebra a, HeytingAlgebra b)
  => HeytingAlgebra (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c)
  => HeytingAlgebra (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c, HeytingAlgebra d)
  => HeytingAlgebra (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c, HeytingAlgebra d,
            HeytingAlgebra e)
  => HeytingAlgebra (a, b, c, d, e)

-- |
deriving newtype instance HeytingAlgebra All

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Bool)) instance HeytingAlgebra Bool

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Char)) instance HeytingAlgebra Char

-- | Newtype derived.
deriving newtype instance HeytingAlgebra a => HeytingAlgebra (Const a b)

-- |
instance CoHeytingAlgebra a => HeytingAlgebra (Down a) where
  (==>) = coerce (flip (\\\) :: a -> a -> a)

-- | Newtype derived.
deriving newtype instance HeytingAlgebra a => HeytingAlgebra (Identity a)

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Int)) instance HeytingAlgebra Int

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Ordering)) instance HeytingAlgebra Ordering

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Word)) instance HeytingAlgebra Word

instance (HeytingAlgebra a, HeytingAlgebra b)
  => (HeytingAlgebra (Lexicographic (a, b))) where

  (==>)
    x@(Lexicographic (a1, b1))
    y@(Lexicographic (a2, b2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              b' | a' == a2  = b1 ==> b2
                 | otherwise = top
           in Lexicographic (a', b')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c)
  => (HeytingAlgebra (Lexicographic (a, b, c))) where

  (==>)
    x@(Lexicographic (a1, b1, c1))
    y@(Lexicographic (a2, b2, c2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              Lexicographic (b', c')
                | a' == a2 =
                    (==>) (Lexicographic (b1, c1))
                          (Lexicographic (b2, c2))
                | otherwise = top
           in Lexicographic (a', b', c')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d)
  => (HeytingAlgebra (Lexicographic (a, b, c, d))) where

  (==>)
    x@(Lexicographic (a1, b1, c1, d1))
    y@(Lexicographic (a2, b2, c2, d2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              Lexicographic (b', c', d')
                | a' == a2 =
                    (==>) (Lexicographic (b1, c1, d1))
                          (Lexicographic (b2, c2, d2))
                | otherwise = top
           in Lexicographic (a', b', c', d')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d, HeytingAlgebra e)
  => (HeytingAlgebra (Lexicographic (a, b, c, d, e))) where

  (==>)
    x@(Lexicographic (a1, b1, c1, d1, e1))
    y@(Lexicographic (a2, b2, c2, d2, e2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              Lexicographic (b', c', d', e')
                | a' == a2 =
                    (==>) (Lexicographic (b1, c1, d1, e1))
                          (Lexicographic (b2, c2, d2, e2))
                | otherwise = top
           in Lexicographic (a', b', c', d', e')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b)
  => (HeytingAlgebra (CoLexicographic (a, b))) where

  (==>)
    x@(CoLexicographic (a1, b1))
    y@(CoLexicographic (a2, b2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              b' | a' == a2  = b1 ==> b2
                 | otherwise = top
           in CoLexicographic (a', b')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c)
  => (HeytingAlgebra (CoLexicographic (a, b, c))) where

  (==>)
    x@(CoLexicographic (a1, b1, c1))
    y@(CoLexicographic (a2, b2, c2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              CoLexicographic (b', c')
                | a' == a2 =
                    (==>) (CoLexicographic (b1, c1))
                          (CoLexicographic (b2, c2))
                | otherwise = top
           in CoLexicographic (a', b', c')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d)
  => (HeytingAlgebra (CoLexicographic (a, b, c, d))) where

  (==>)
    x@(CoLexicographic (a1, b1, c1, d1))
    y@(CoLexicographic (a2, b2, c2, d2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              CoLexicographic (b', c', d')
                | a' == a2 =
                    (==>) (CoLexicographic (b1, c1, d1))
                          (CoLexicographic (b2, c2, d2))
                | otherwise = top
           in CoLexicographic (a', b', c', d')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d, HeytingAlgebra e)
  => (HeytingAlgebra (CoLexicographic (a, b, c, d, e))) where

  (==>)
    x@(CoLexicographic (a1, b1, c1, d1, e1))
    y@(CoLexicographic (a2, b2, c2, d2, e2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 ==> a2
              CoLexicographic (b', c', d', e')
                | a' == a2 =
                    (==>) (CoLexicographic (b1, c1, d1, e1))
                          (CoLexicographic (b2, c2, d2, e2))
                | otherwise = top
           in CoLexicographic (a', b', c', d', e')
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b)
  => (HeytingAlgebra (ProductOrder (a, b))) where

  (==>)
    (ProductOrder (a1, b1))
    (ProductOrder (a2, b2)) =
      ProductOrder (a1 ==> a2, b1 ==> b2)
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c)
  => (HeytingAlgebra (ProductOrder (a, b, c))) where

  (==>)
    (ProductOrder (a1, b1, c1))
    (ProductOrder (a2, b2, c2)) =
      ProductOrder (a1 ==> a2, b1 ==> b2, c1 ==> c2)
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d)
  => (HeytingAlgebra (ProductOrder (a, b, c, d))) where

  (==>)
    (ProductOrder (a1, b1, c1, d1))
    (ProductOrder (a2, b2, c2, d2)) =
      ProductOrder (a1 ==> a2, b1 ==> b2, c1 ==> c2, d1 ==> d2)
  {-# INLINE (==>) #-}

instance (HeytingAlgebra a, HeytingAlgebra b, HeytingAlgebra c,
          HeytingAlgebra d, HeytingAlgebra e)
  => (HeytingAlgebra (ProductOrder (a, b, c, d, e))) where

  (==>)
    (ProductOrder (a1, b1, c1, d1, e1))
    (ProductOrder (a2, b2, c2, d2, e2)) =
      ProductOrder (a1 ==> a2, b1 ==> b2, c1 ==> c2, d1 ==> d2, e1 ==> e2)
  {-# INLINE (==>) #-}

--------------------------------------------------------------------------------
-- * Co-Heyting algebras
--------------------------------------------------------------------------------

-- | 
class BoundedLattice a => CoHeytingAlgebra a where
  -- | prop> a \/ x >=? b iff x >=? a \\\ b
  (\\\) :: a -> a -> a

  -- | prop> a \/ x >=? b iff x >=? b /// a
  (///) :: a -> a -> a
  (///) = flip (\\\)

  -- | Least @x@ such that @a \/ x == top@.
  pseudoSupplement :: a -> a
  pseudoSupplement x = top \\\ x
  {-# MINIMAL (\\\) #-}

-- | Synonym for @'(\\\)'@.
subtracts :: CoHeytingAlgebra a => a -> a -> a
subtracts = (\\\)

--------------------------------------------------------------------------------
-- ** Generics
--------------------------------------------------------------------------------

gsubtracts
  :: forall a
   . Generic a
  => CoHeytingAlgebra (Rep a ())
  => a -> a -> a
gsubtracts = \x y -> to (subtracts (from' x) (from' y))
  where
  from' :: a -> Rep a ()
  from' = from

instance CoHeytingAlgebra (U1 a) where
  (\\\) U1 U1 = U1

-- |
instance CoHeytingAlgebra c => CoHeytingAlgebra (K1 i c p) where
  (\\\) = coerce ((\\\) :: c -> c -> c)

-- |
instance CoHeytingAlgebra (f p) => CoHeytingAlgebra (M1 i c f p) where
  (\\\) = coerce ((\\\) :: f p -> f p -> f p)

-- | Ordinal sum ordering to match the @'Ord' ('(:+:)' f g p)@ instance.
instance (CoHeytingAlgebra (f p), CoHeytingAlgebra (g p))
  => CoHeytingAlgebra ((f :+: g) p) where

  (\\\) x y
    | L1 a <- x, L1 b <- y = L1 (a \\\ b)
    | R1 a <- x, R1 b <- y = R1 (a \\\ b)
    | otherwise            = bottom

--------------------------------------------------------------------------------
-- ** Instances
--------------------------------------------------------------------------------

-- | Get a @'CoHeytingAlgebra'@ instance for @'Bounded'@, @'Ord'@ @a@.
instance (Ord a, Bounded a)
  => CoHeytingAlgebra (WrappedBounded (WrappedOrd a)) where

  (\\\) x y
    | x >= y    = bottom
    | otherwise = y

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via WrappedBounded (WrappedOrd (a :~: b))
  instance (a ~ b) => CoHeytingAlgebra (a :~: b)

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via WrappedBounded (WrappedOrd ()) instance CoHeytingAlgebra ()

-- |
deriving newtype instance CoHeytingAlgebra Any

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Bool)) instance CoHeytingAlgebra Bool

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Char)) instance CoHeytingAlgebra Char

-- | Newtype derived.
deriving newtype instance CoHeytingAlgebra a => CoHeytingAlgebra (Const a b)

instance HeytingAlgebra a => CoHeytingAlgebra (Down a) where
  (\\\) = coerce (flip (==>) :: a -> a -> a)

-- | Newtype derived.
deriving newtype instance CoHeytingAlgebra a => CoHeytingAlgebra (Identity a)

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Int)) instance CoHeytingAlgebra Int

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Ordering)) instance CoHeytingAlgebra Ordering

-- | Derived via @'WrappedBounded' ('WrappedOrd' a)@.
deriving via (WrappedBounded (WrappedOrd Word)) instance CoHeytingAlgebra Word

instance (CoHeytingAlgebra a, CoHeytingAlgebra b)
  => (CoHeytingAlgebra (Lexicographic (a, b))) where

  (\\\)
    x@(Lexicographic (a1, b1))
    y@(Lexicographic (a2, b2))
      | x >=? y   = top
      | otherwise =
          let a' = a1 \\\ a2
              b' | a' == a2  = b1 \\\ b2
                 | otherwise = top
           in Lexicographic (a', b')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c)
  => (CoHeytingAlgebra (Lexicographic (a, b, c))) where

  (\\\)
    x@(Lexicographic (a1, b1, c1))
    y@(Lexicographic (a2, b2, c2))
      | x >=? y   = top
      | otherwise =
          let a' = a1 \\\ a2
              Lexicographic (b', c')
                | a' == a2 =
                    (\\\) (Lexicographic (b1, c1))
                          (Lexicographic (b2, c2))
                | otherwise = top
           in Lexicographic (a', b', c')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d)
  => (CoHeytingAlgebra (Lexicographic (a, b, c, d))) where

  (\\\)
    x@(Lexicographic (a1, b1, c1, d1))
    y@(Lexicographic (a2, b2, c2, d2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 \\\ a2
              Lexicographic (b', c', d')
                | a' == a2 =
                    (\\\) (Lexicographic (b1, c1, d1))
                          (Lexicographic (b2, c2, d2))
                | otherwise = top
           in Lexicographic (a', b', c', d')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d, CoHeytingAlgebra e)
  => (CoHeytingAlgebra (Lexicographic (a, b, c, d, e))) where

  (\\\)
    x@(Lexicographic (a1, b1, c1, d1, e1))
    y@(Lexicographic (a2, b2, c2, d2, e2))
      | x <=? y   = top
      | otherwise =
          let a' = a1 \\\ a2
              Lexicographic (b', c', d', e')
                | a' == a2 =
                    (\\\) (Lexicographic (b1, c1, d1, e1))
                          (Lexicographic (b2, c2, d2, e2))
                | otherwise = top
           in Lexicographic (a', b', c', d', e')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b)
  => (CoHeytingAlgebra (CoLexicographic (a, b))) where

  (\\\)
    x@(CoLexicographic (a1, b1))
    y@(CoLexicographic (a2, b2))
      | x >=? y   = top
      | otherwise =
          let b' = b1 \\\ b2
              a' | b' == b2  = a1 \\\ a2
                 | otherwise = top
           in CoLexicographic (a', b')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c)
  => (CoHeytingAlgebra (CoLexicographic (a, b, c))) where

  (\\\)
    x@(CoLexicographic (a1, b1, c1))
    y@(CoLexicographic (a2, b2, c2))
      | x >=? y   = top
      | otherwise =
          let c' = c1 \\\ c2
              CoLexicographic (a', b')
                | c' == c2 =
                    (\\\) (CoLexicographic (a1, b1))
                          (CoLexicographic (a2, b2))
                | otherwise = top
           in CoLexicographic (a', b', c')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d)
  => (CoHeytingAlgebra (CoLexicographic (a, b, c, d))) where

  (\\\)
    x@(CoLexicographic (a1, b1, c1, d1))
    y@(CoLexicographic (a2, b2, c2, d2))
      | x <=? y   = top
      | otherwise =
          let d' = d1 \\\ d2
              CoLexicographic (a', b', c')
                | d' == d2 =
                    (\\\) (CoLexicographic (a1, b1, c1))
                          (CoLexicographic (a2, b2, c2))
                | otherwise = top
           in CoLexicographic (a', b', c', d')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d, CoHeytingAlgebra e)
  => (CoHeytingAlgebra (CoLexicographic (a, b, c, d, e))) where

  (\\\)
    x@(CoLexicographic (a1, b1, c1, d1, e1))
    y@(CoLexicographic (a2, b2, c2, d2, e2))
      | x <=? y   = top
      | otherwise =
          let e' = e1 \\\ e2
              CoLexicographic (a', b', c', d')
                | e' == e2 =
                    (\\\) (CoLexicographic (a1, b1, c1, d1))
                          (CoLexicographic (a2, b2, c2, d2))
                | otherwise = top
           in CoLexicographic (a', b', c', d', e')
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b)
  => (CoHeytingAlgebra (ProductOrder (a, b))) where

  (\\\)
    (ProductOrder (a1, b1))
    (ProductOrder (a2, b2)) =
      ProductOrder (a1 \\\ a2, b1 \\\ b2)
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c)
  => (CoHeytingAlgebra (ProductOrder (a, b, c))) where

  (\\\)
    (ProductOrder (a1, b1, c1))
    (ProductOrder (a2, b2, c2)) =
      ProductOrder (a1 \\\ a2, b1 \\\ b2, c1 \\\ c2)
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d)
  => (CoHeytingAlgebra (ProductOrder (a, b, c, d))) where

  (\\\)
    (ProductOrder (a1, b1, c1, d1))
    (ProductOrder (a2, b2, c2, d2)) =
      ProductOrder (a1 \\\ a2, b1 \\\ b2, c1 \\\ c2, d1 \\\ d2)
  {-# INLINE (\\\) #-}

instance (CoHeytingAlgebra a, CoHeytingAlgebra b, CoHeytingAlgebra c,
          CoHeytingAlgebra d, CoHeytingAlgebra e)
  => (CoHeytingAlgebra (ProductOrder (a, b, c, d, e))) where

  (\\\)
    (ProductOrder (a1, b1, c1, d1, e1))
    (ProductOrder (a2, b2, c2, d2, e2)) =
      ProductOrder (a1 \\\ a2, b1 \\\ b2, c1 \\\ c2, d1 \\\ d2, e1 \\\ e2)
  {-# INLINE (\\\) #-}

--------------------------------------------------------------------------------
-- * Bi-Heyting algebras
--------------------------------------------------------------------------------

type BiHeytingAlgebra a = (HeytingAlgebra a, CoHeytingAlgebra a)
