{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Ordering.Extrema
  (
  -- * Least elements
    LeastElement(..)

  -- ** Generics
  , gbottom

  -- * Greatest elements
  , GreatestElement(..)

  -- ** Generics
  , gtop

  -- * Bounded wrapper
  , WrappedBounded(..)
  ) where

import Control.Lens          (iso, Wrapped(..), Rewrapped)
import Data.Coerce           (coerce)
import Data.Functor.Const    (Const(..))
import Data.Functor.Identity (Identity(..))
import Data.Semigroup        (All(..), Any(..), Dual(..), First(..),
                              Last(..), Max(..), Min(..), Option(..),
                              Product(..), Sum(..)) 
import Data.Type.Equality    ((:~:))
import Data.Ord              (Down(..))
import GHC.Generics          (Generic(..), U1(..), K1(..), M1(..), (:+:)(..),
                              (:*:)(..))
import Ordering.PartialOrd

--------------------------------------------------------------------------------
-- * Least elements
--------------------------------------------------------------------------------

-- | A partial ordering with a least element, bottom.
--
-- prop> forall x. bottom <= x
class PartialOrd a => LeastElement a where
  bottom :: a

--------------------------------------------------------------------------------
-- ** Generics
--------------------------------------------------------------------------------

-- |
gbottom
  :: forall a
   . Generic a
  => LeastElement (Rep a ())
  => a
gbottom = to (bottom :: Rep a ())

-- |
instance LeastElement (U1 a) where
  bottom = U1

-- |
instance LeastElement c => LeastElement (K1 i c p) where
  bottom = K1 bottom

-- |
instance LeastElement (f p) => LeastElement (M1 i c f p) where
  bottom = M1 bottom

-- |
instance (LeastElement (f p), PartialOrd (g p))
  => LeastElement ((f :+: g) p) where

  bottom = L1 bottom

-- | Generalises @'Bounded' ('(:*:)' f g p)@ instance.
instance (LeastElement (f p), LeastElement (g p))
  => LeastElement ((f :*: g) p)
  where

  bottom = bottom :*: bottom

--------------------------------------------------------------------------------
-- ** Other instances
--------------------------------------------------------------------------------

-- | Newtype derived.
deriving newtype instance (LeastElement a, Ord a) => LeastElement (WrappedOrd a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded (a :~: b) instance (a ~ b) => LeastElement (a :~: b)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded () instance LeastElement ()

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b)
  instance (LeastElement a, LeastElement b)
  => LeastElement (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (LeastElement a, LeastElement b, LeastElement c)
  => LeastElement (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d)
  => LeastElement (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d,
            LeastElement e)
  => LeastElement (a, b, c, d, e)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd All instance LeastElement All

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Any instance LeastElement Any

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Bool instance LeastElement Bool

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Char instance LeastElement Char

-- | Newtype derived.
deriving newtype instance LeastElement a => LeastElement (Const a b)

-- | Generalises @'Bounded' ('Down' a)@.
instance GreatestElement a => LeastElement (Down a) where
  bottom = Down top

-- |
deriving newtype instance LeastElement a => LeastElement (Dual a)

-- | Generalises @'Bounded' ('Either' a b)@.
instance (LeastElement a, PartialOrd b) => LeastElement (Either a b) where
  bottom = Left bottom

-- |
deriving newtype instance LeastElement a => LeastElement (First a)

-- | Newtype derived.
deriving newtype instance LeastElement a => LeastElement (Identity a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Int instance LeastElement Int

-- |
deriving newtype instance LeastElement a => LeastElement (Last a)

-- | Generalises @'Bounded' ('Maybe' a)@.
instance PartialOrd a => LeastElement (Maybe a) where
  bottom = Nothing

-- |
deriving newtype instance LeastElement a => LeastElement (Max a)

-- |
deriving newtype instance LeastElement a => LeastElement (Min a)

-- |
deriving newtype instance LeastElement a => LeastElement (Option a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Ordering instance LeastElement Ordering

-- |
deriving newtype instance LeastElement a => LeastElement (Product a)

-- |
deriving newtype instance LeastElement a => LeastElement (Sum a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Word instance LeastElement Word

-- |
instance (LeastElement a, LeastElement b)
  => LeastElement (Lexicographic (a, b)) where

  bottom = Lexicographic (bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c)
  => LeastElement (Lexicographic (a, b, c)) where

  bottom = Lexicographic (bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d)
  => LeastElement (Lexicographic (a, b, c, d)) where

  bottom = Lexicographic (bottom, bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d,
          LeastElement e)
  => LeastElement (Lexicographic (a, b, c, d, e)) where

  bottom = Lexicographic (bottom, bottom, bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b)
  => LeastElement (CoLexicographic (a, b)) where

  bottom = CoLexicographic (bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c)
  => LeastElement (CoLexicographic (a, b, c)) where

  bottom = CoLexicographic (bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d)
  => LeastElement (CoLexicographic (a, b, c, d)) where

  bottom = CoLexicographic (bottom, bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d,
          LeastElement e)
  => LeastElement (CoLexicographic (a, b, c, d, e)) where

  bottom = CoLexicographic (bottom, bottom, bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b)
  => LeastElement (ProductOrder (a, b)) where

  bottom = ProductOrder (bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c)
  => LeastElement (ProductOrder (a, b, c)) where

  bottom = ProductOrder (bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d)
  => LeastElement (ProductOrder (a, b, c, d)) where

  bottom = ProductOrder (bottom, bottom, bottom, bottom)

-- |
instance (LeastElement a, LeastElement b, LeastElement c, LeastElement d,
          LeastElement e)
  => LeastElement (ProductOrder (a, b, c, d, e)) where

  bottom = ProductOrder (bottom, bottom, bottom, bottom, bottom)

--------------------------------------------------------------------------------
-- * Greatest elements
--------------------------------------------------------------------------------

-- | A partial ordering with a greatest element, top.
--
-- prop> forall x. x <= top
class PartialOrd a => GreatestElement a where
  top :: a

--------------------------------------------------------------------------------
-- ** Generics
--------------------------------------------------------------------------------

-- |
gtop
  :: forall a
   . Generic a
  => GreatestElement (Rep a ())
  => a
gtop = to (top :: Rep a ())

-- |
instance GreatestElement (U1 a) where
  top = U1

-- |
instance GreatestElement c => GreatestElement (K1 i c p) where
  top = K1 top

-- |
instance GreatestElement (f p) => GreatestElement (M1 i c f p) where
  top = M1 top

-- |
instance (PartialOrd (f p), GreatestElement (g p))
  => GreatestElement ((f :+: g) p) where

  top = R1 top

-- | Generalises @'Bounded' ('(:*:)' f g p)@ instance.
instance (GreatestElement (f p), GreatestElement (g p))
  => GreatestElement ((f :*: g) p) where

  top = top :*: top

--------------------------------------------------------------------------------
-- ** Other instances
--------------------------------------------------------------------------------

-- |
deriving newtype
  instance (GreatestElement a, Ord a) => GreatestElement (WrappedOrd a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded (a :~: b)
  instance (a ~ b) => GreatestElement (a :~: b)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded () instance GreatestElement ()

-- | Derived via @'WrappedOrd'@.
deriving via Lexicographic (a, b)
  instance (GreatestElement a, GreatestElement b)
  => GreatestElement (a, b)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c)
  instance (GreatestElement a, GreatestElement b, GreatestElement c)
  => GreatestElement (a, b, c)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d)
  instance (GreatestElement a, GreatestElement b, GreatestElement c,
            GreatestElement d)
  => GreatestElement (a, b, c, d)

-- | Derived via @'Lexicographic'@.
deriving via Lexicographic (a, b, c, d, e)
  instance (GreatestElement a, GreatestElement b, GreatestElement c,
            GreatestElement d, GreatestElement e)
  => GreatestElement (a, b, c, d, e)

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd All instance GreatestElement All

-- | Derived via @'WrappedOrd'@.
deriving via WrappedOrd Any instance GreatestElement Any

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Bool instance GreatestElement Bool

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Char instance GreatestElement Char

-- | Newtype derived.
deriving newtype instance GreatestElement a => GreatestElement (Const a b)

-- | Generalises @'Bounded' ('Down' a)@.
instance LeastElement a => GreatestElement (Down a) where
  top = Down bottom

-- |
deriving newtype instance GreatestElement a => GreatestElement (Dual a)

-- | Generalises @'Bounded' ('Either' a b)@.
instance (GreatestElement a, PartialOrd b) => GreatestElement (Either a b) where
  top = Left top

-- |
deriving newtype instance GreatestElement a => GreatestElement (First a)

-- | Newtype derived.
deriving newtype instance GreatestElement a => GreatestElement (Identity a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Int instance GreatestElement Int

-- |
deriving newtype instance GreatestElement a => GreatestElement (Last a)

-- | Generalises @'Bounded' ('Maybe' a)@.
instance GreatestElement a => GreatestElement (Maybe a) where
  top = Just top

-- |
deriving newtype instance GreatestElement a => GreatestElement (Max a)

-- |
deriving newtype instance GreatestElement a => GreatestElement (Min a)

-- |
deriving newtype instance GreatestElement a => GreatestElement (Option a)
-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Ordering instance GreatestElement Ordering

-- |
deriving newtype instance GreatestElement a => GreatestElement (Product a)

-- |
deriving newtype instance GreatestElement a => GreatestElement (Sum a)

-- | Derived via @'WrappedBounded'@.
deriving via WrappedBounded Word instance GreatestElement Word

-- |
instance (GreatestElement a, GreatestElement b)
  => GreatestElement (Lexicographic (a, b)) where

  top = Lexicographic (top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c)
  => GreatestElement (Lexicographic (a, b, c)) where

  top = Lexicographic (top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d)
  => GreatestElement (Lexicographic (a, b, c, d)) where

  top = Lexicographic (top, top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d, GreatestElement e)
  => GreatestElement (Lexicographic (a, b, c, d, e)) where

  top = Lexicographic (top, top, top, top, top)

-- |
instance (GreatestElement a, GreatestElement b)
  => GreatestElement (CoLexicographic (a, b)) where

  top = CoLexicographic (top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c)
  => GreatestElement (CoLexicographic (a, b, c)) where

  top = CoLexicographic (top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d)
  => GreatestElement (CoLexicographic (a, b, c, d)) where

  top = CoLexicographic (top, top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d, GreatestElement e)
  => GreatestElement (CoLexicographic (a, b, c, d, e)) where

  top = CoLexicographic (top, top, top, top, top)

-- |
instance (GreatestElement a, GreatestElement b)
  => GreatestElement (ProductOrder (a, b)) where

  top = ProductOrder (top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c)
  => GreatestElement (ProductOrder (a, b, c)) where

  top = ProductOrder (top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d)
  => GreatestElement (ProductOrder (a, b, c, d)) where

  top = ProductOrder (top, top, top, top)

-- |
instance (GreatestElement a, GreatestElement b, GreatestElement c,
          GreatestElement d, GreatestElement e)
  => GreatestElement (ProductOrder (a, b, c, d, e)) where

  top = ProductOrder (top, top, top, top, top)

--------------------------------------------------------------------------------
-- * Wrapped `Bounded` instances
--------------------------------------------------------------------------------

-- | Get a @'LowerBounded'@ and @'UpperBounded'@ instance for any @a@ with an
-- @'Bounded'@ instance.
newtype WrappedBounded a = WrappedBounded a
  deriving (Show, Read, Eq, Ord, Bounded)
  deriving newtype (PartialOrd)

instance (t ~ WrappedBounded a) => Rewrapped (WrappedBounded a) t
instance Wrapped (WrappedBounded a) where
  type Unwrapped (WrappedBounded a) = a
  _Wrapped' = iso coerce coerce
  {-# INLINE _Wrapped' #-}

-- | Get a @'LeastElement'@ instance for @'Bounded'@ @a@ with @'PartialOrd'@
-- instance.
instance (PartialOrd a, Bounded a) => LeastElement (WrappedBounded a) where
  bottom = minBound

-- | Get a @'GreatestElement'@ instance for @'Bounded'@ @a@ with @'PartialOrd'@
-- instance.
instance (PartialOrd a, Bounded a) => GreatestElement (WrappedBounded a) where
  top = maxBound
